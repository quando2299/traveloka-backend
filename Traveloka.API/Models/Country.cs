﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Country
    {
        public Country()
        {
            Airports = new HashSet<Airport>();
        }

        public string CountryCode { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Airport> Airports { get; set; }
    }
}
