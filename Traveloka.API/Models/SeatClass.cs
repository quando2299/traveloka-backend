﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class SeatClass
    {
        public SeatClass()
        {
            AircraftSeats = new HashSet<AircraftSeat>();
        }

        public int ClassId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AircraftSeat> AircraftSeats { get; set; }
    }
}
