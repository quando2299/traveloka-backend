﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Passenger
    {
        public Passenger()
        {
            PassengersOnFlights = new HashSet<PassengersOnFlight>();
            Transactions = new HashSet<Transaction>();
        }

        public int PersonId { get; set; }
        public string TsaRedressNum { get; set; }
        public string KnownTravelerNum { get; set; }

        public virtual Person Person { get; set; }
        public virtual ICollection<PassengersOnFlight> PassengersOnFlights { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
