﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class TypeTransaction
    {
        public TypeTransaction()
        {
            Transactions = new HashSet<Transaction>();
        }

        public int Type { get; set; }
        public string Status { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
