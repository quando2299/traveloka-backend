﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class RouteStatus
    {
        public RouteStatus()
        {
            Flights = new HashSet<Flight>();
        }

        public int StatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Flight> Flights { get; set; }
    }
}
