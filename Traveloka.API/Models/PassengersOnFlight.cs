﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class PassengersOnFlight
    {
        public string ItineraryId { get; set; }
        public int RouteId { get; set; }
        public DateTime DepartTimestamp { get; set; }
        public int PersonId { get; set; }
        public string SeatNum { get; set; }

        public virtual Passenger Person { get; set; }
    }
}
