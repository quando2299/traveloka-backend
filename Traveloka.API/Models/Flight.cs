﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Flight
    {
        public int RouteId { get; set; }
        public DateTime DepartTimestamp { get; set; }
        public DateTime ArriveTimestamp { get; set; }
        public decimal BasePriceUsd { get; set; }
        public int StatusId { get; set; }

        public virtual Route Route { get; set; }
        public virtual RouteStatus Status { get; set; }
    }
}
