﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class travelokaContext : DbContext
    {
        //public travelokaContext()
        //{
        //}

        public travelokaContext(DbContextOptions<travelokaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Aircraft> Aircrafts { get; set; }
        public virtual DbSet<AircraftSeat> AircraftSeats { get; set; }
        public virtual DbSet<Airport> Airports { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Flight> Flights { get; set; }
        public virtual DbSet<Itinerary> Itineraries { get; set; }
        public virtual DbSet<Passenger> Passengers { get; set; }
        public virtual DbSet<PassengersOnFlight> PassengersOnFlights { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Route> Routes { get; set; }
        public virtual DbSet<RouteStatus> RouteStatuses { get; set; }
        public virtual DbSet<SeatClass> SeatClasses { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TypeTransaction> TypeTransactions { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Data Source=den1.mssql7.gear.host;Initial Catalog=traveloka;Persist Security Info=True;User ID=traveloka;Password=Sg63Yo!2n12?");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Aircraft>(entity =>
            {
                entity.HasIndex(e => e.AircraftId, "UQ__Aircraft__04015398EBF65D2C")
                    .IsUnique();

                entity.Property(e => e.AircraftId).HasColumnName("aircraft_id");

                entity.Property(e => e.Manufacturer)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("manufacturer");

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("model");
            });

            modelBuilder.Entity<AircraftSeat>(entity =>
            {
                entity.HasKey(e => new { e.AircraftId, e.SeatNum })
                    .HasName("PK__Aircraft__17EC94B40752BAFC");

                entity.HasIndex(e => e.AircraftId, "UQ__Aircraft__040153984DDE36ED")
                    .IsUnique();

                entity.Property(e => e.AircraftId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("aircraft_id");

                entity.Property(e => e.SeatNum)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("seat_num");

                entity.Property(e => e.ClassId).HasColumnName("class_id");

                entity.HasOne(d => d.Aircraft)
                    .WithOne(p => p.AircraftSeat)
                    .HasForeignKey<AircraftSeat>(d => d.AircraftId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AircraftS__aircr__6C190EBB");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.AircraftSeats)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AircraftS__class__6D0D32F4");
            });

            modelBuilder.Entity<Airport>(entity =>
            {
                entity.HasKey(e => e.IataCode)
                    .HasName("PK__Airports__1B78975D8A4531B6");

                entity.HasIndex(e => e.IataCode, "UQ__Airports__1B78975C5157AC48")
                    .IsUnique();

                entity.Property(e => e.IataCode)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("iata_code")
                    .IsFixedLength(true);

                entity.Property(e => e.AirportName)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("airport_name");

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("country_code")
                    .IsFixedLength(true);

                entity.HasOne(d => d.CountryCodeNavigation)
                    .WithMany(p => p.Airports)
                    .HasForeignKey(d => d.CountryCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Airports__countr__73BA3083");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasKey(e => e.CountryCode)
                    .HasName("PK__Countrie__3436E9A4F26B52FC");

                entity.HasIndex(e => e.CountryCode, "UQ__Countrie__3436E9A50164B609")
                    .IsUnique();

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("country_code")
                    .IsFixedLength(true);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.PersonId)
                    .HasName("PK__Employee__543848DF52B1A52A");

                entity.HasIndex(e => e.PersonId, "UQ__Employee__543848DE5A4955FA")
                    .IsUnique();

                entity.Property(e => e.PersonId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("person_id");

                entity.Property(e => e.HireDate)
                    .HasColumnType("date")
                    .HasColumnName("hire_date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HourlyWageUsd)
                    .HasColumnType("money")
                    .HasColumnName("hourly_wage_usd");

                entity.HasOne(d => d.Person)
                    .WithOne(p => p.Employee)
                    .HasForeignKey<Employee>(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employees__perso__5165187F");
            });

            modelBuilder.Entity<Flight>(entity =>
            {
                entity.HasKey(e => new { e.RouteId, e.DepartTimestamp })
                    .HasName("PK__Flights__E92432040ED94CF6");

                entity.Property(e => e.RouteId).HasColumnName("route_id");

                entity.Property(e => e.DepartTimestamp)
                    .HasColumnType("datetime")
                    .HasColumnName("depart_timestamp");

                entity.Property(e => e.ArriveTimestamp)
                    .HasColumnType("datetime")
                    .HasColumnName("arrive_timestamp");

                entity.Property(e => e.BasePriceUsd)
                    .HasColumnType("money")
                    .HasColumnName("base_price_usd");

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.HasOne(d => d.Route)
                    .WithMany(p => p.Flights)
                    .HasForeignKey(d => d.RouteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Flights__route_i__08B54D69");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Flights)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Flights__status___09A971A2");
            });

            modelBuilder.Entity<Itinerary>(entity =>
            {
                entity.HasIndex(e => e.ItineraryId, "UQ__Itinerar__6E8B21D75801189D")
                    .IsUnique();

                entity.Property(e => e.ItineraryId)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .HasColumnName("itinerary_id")
                    .IsFixedLength(true);

                entity.Property(e => e.PerPersonPrice)
                    .HasColumnType("money")
                    .HasColumnName("per_person_price");
            });

            modelBuilder.Entity<Passenger>(entity =>
            {
                entity.HasKey(e => e.PersonId)
                    .HasName("PK__Passenge__543848DFF1917DA6");

                entity.HasIndex(e => e.KnownTravelerNum, "UQ__Passenge__12003587D3ABD311")
                    .IsUnique();

                entity.HasIndex(e => e.TsaRedressNum, "UQ__Passenge__7379E5868C46F82B")
                    .IsUnique();

                entity.Property(e => e.PersonId)
                    .ValueGeneratedNever()
                    .HasColumnName("person_id");

                entity.Property(e => e.KnownTravelerNum)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("known_traveler_num")
                    .HasDefaultValueSql("('')")
                    .IsFixedLength(true);

                entity.Property(e => e.TsaRedressNum)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .HasColumnName("tsa_redress_num")
                    .HasDefaultValueSql("('')")
                    .IsFixedLength(true);

                entity.HasOne(d => d.Person)
                    .WithOne(p => p.Passenger)
                    .HasForeignKey<Passenger>(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Passenger__perso__5EBF139D");
            });

            modelBuilder.Entity<PassengersOnFlight>(entity =>
            {
                entity.HasKey(e => new { e.ItineraryId, e.RouteId, e.DepartTimestamp, e.PersonId })
                    .HasName("PK__Passenge__BDFC2172A7CB5FBB");

                entity.Property(e => e.ItineraryId)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .HasColumnName("itinerary_id")
                    .IsFixedLength(true);

                entity.Property(e => e.RouteId).HasColumnName("route_id");

                entity.Property(e => e.DepartTimestamp)
                    .HasColumnType("datetime")
                    .HasColumnName("depart_timestamp");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.Property(e => e.SeatNum)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("seat_num")
                    .IsFixedLength(true);

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PassengersOnFlights)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Passenger__perso__10566F31");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasIndex(e => e.PersonId, "UQ__Persons__543848DE6761312D")
                    .IsUnique();

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnType("date")
                    .HasColumnName("date_of_birth");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("first_name");

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("gender")
                    .IsFixedLength(true);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("last_name");

                entity.Property(e => e.MiddleName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("middle_name");

                entity.Property(e => e.Password)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PhonePrimary)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("phone_primary")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Route>(entity =>
            {
                entity.ToTable("Route");

                entity.HasIndex(e => e.RouteId, "UQ__Route__28F706FF4621E42F")
                    .IsUnique();

                entity.Property(e => e.RouteId).HasColumnName("route_id");

                entity.Property(e => e.AircraftId).HasColumnName("aircraft_id");

                entity.Property(e => e.DestinationAirport)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("destination_airport")
                    .IsFixedLength(true);

                entity.Property(e => e.DistanceMiles).HasColumnName("distance_miles");

                entity.Property(e => e.FlightNum).HasColumnName("flight_num");

                entity.Property(e => e.OriginAirport)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("origin_airport")
                    .IsFixedLength(true);

                entity.HasOne(d => d.DestinationAirportNavigation)
                    .WithMany(p => p.RouteDestinationAirportNavigations)
                    .HasForeignKey(d => d.DestinationAirport)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Route__destinati__02FC7413");

                entity.HasOne(d => d.OriginAirportNavigation)
                    .WithMany(p => p.RouteOriginAirportNavigations)
                    .HasForeignKey(d => d.OriginAirport)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Route__origin_ai__02084FDA");
            });

            modelBuilder.Entity<RouteStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PK__RouteSta__3683B531652F47AE");

                entity.HasIndex(e => e.StatusId, "UQ__RouteSta__3683B53095FD19CA")
                    .IsUnique();

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<SeatClass>(entity =>
            {
                entity.HasKey(e => e.ClassId)
                    .HasName("PK__SeatClas__FDF47986823223BB");

                entity.HasIndex(e => e.ClassId, "UQ__SeatClas__FDF47987EB528DCF")
                    .IsUnique();

                entity.Property(e => e.ClassId).HasColumnName("class_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.HasKey(e => new { e.PassengerId, e.RouteId })
                    .HasName("PK__Transact__4098263AB8F52907");

                entity.Property(e => e.PassengerId).HasColumnName("PassengerID");

                entity.Property(e => e.RouteId).HasColumnName("RouteID");

                entity.Property(e => e.BookingDate).HasColumnType("datetime");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.Passenger)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.PassengerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Transacti__Passe__3864608B");

                entity.HasOne(d => d.Route)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.RouteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Transacti__Route__395884C4");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.Type)
                    .HasConstraintName("FK__Transactio__type__37703C52");
            });

            modelBuilder.Entity<TypeTransaction>(entity =>
            {
                entity.HasKey(e => e.Type)
                    .HasName("PK__TypeTran__E3F85249C87A5153");

                entity.ToTable("TypeTransaction");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("status");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
