﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class AircraftSeat
    {
        public int AircraftId { get; set; }
        public string SeatNum { get; set; }
        public int ClassId { get; set; }

        public virtual Aircraft Aircraft { get; set; }
        public virtual SeatClass Class { get; set; }
    }
}
