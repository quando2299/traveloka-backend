﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Employee
    {
        public int PersonId { get; set; }
        public DateTime HireDate { get; set; }
        public decimal HourlyWageUsd { get; set; }

        public virtual Person Person { get; set; }
    }
}
