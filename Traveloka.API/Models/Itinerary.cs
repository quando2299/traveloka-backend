﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Itinerary
    {
        public string ItineraryId { get; set; }
        public decimal PerPersonPrice { get; set; }
    }
}
