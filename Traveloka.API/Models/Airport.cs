﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Airport
    {
        public Airport()
        {
            RouteDestinationAirportNavigations = new HashSet<Route>();
            RouteOriginAirportNavigations = new HashSet<Route>();
        }

        public string IataCode { get; set; }
        public string AirportName { get; set; }
        public string CountryCode { get; set; }

        public virtual Country CountryCodeNavigation { get; set; }
        public virtual ICollection<Route> RouteDestinationAirportNavigations { get; set; }
        public virtual ICollection<Route> RouteOriginAirportNavigations { get; set; }
    }
}
