﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Route
    {
        public Route()
        {
            Flights = new HashSet<Flight>();
            Transactions = new HashSet<Transaction>();
        }

        public int RouteId { get; set; }
        public short FlightNum { get; set; }
        public string OriginAirport { get; set; }
        public string DestinationAirport { get; set; }
        public int AircraftId { get; set; }
        public short DistanceMiles { get; set; }

        public virtual Airport DestinationAirportNavigation { get; set; }
        public virtual Airport OriginAirportNavigation { get; set; }
        public virtual ICollection<Flight> Flights { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
