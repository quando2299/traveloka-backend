﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Transaction
    {
        public int? Type { get; set; }
        public DateTime? BookingDate { get; set; }
        public int PassengerId { get; set; }
        public int RouteId { get; set; }
        public bool? IsPaid { get; set; }
        public int? PaymentMethod { get; set; }
        public int? Amount { get; set; }

        public virtual Passenger Passenger { get; set; }
        public virtual Route Route { get; set; }
        public virtual TypeTransaction TypeNavigation { get; set; }
    }
}
