﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Traveloka.API.Models
{
    public partial class Aircraft
    {
        public int AircraftId { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }

        public virtual AircraftSeat AircraftSeat { get; set; }
    }
}
