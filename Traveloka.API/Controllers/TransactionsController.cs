﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Traveloka.API.DTO.Transactions;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly travelokaContext _context;

        public TransactionsController(travelokaContext context)
        {
            _context = context;
        }
    }
}
