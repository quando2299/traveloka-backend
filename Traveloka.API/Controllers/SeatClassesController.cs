﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Traveloka.API.DTO.SeatClasses;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeatClassesController : ControllerBase
    {
        private readonly travelokaContext _context;

        public SeatClassesController(travelokaContext context)
        {
            _context = context;
        }

        // GET: api/SeatClasses
        [HttpGet]
        public IQueryable<SeatClassesDTO> GetSeatClasses()
        {
            var classes = from a in _context.SeatClasses
                          select new SeatClassesDTO()
                          {
                              ClassId = a.ClassId,
                              Name = a.Name
                          };
            return classes;
        }

        // GET: api/SeatClasses/5
        [HttpGet("{id}")]
        public SeatClassesDTO GetSeatClass(int id)
        {
            var list = this._context.SeatClasses.ToList();
            var detail = from a in list
                         where a.ClassId == id
                         select new SeatClassesDTO()
                         {
                             ClassId = a.ClassId,
                             Name = a.Name
                         };

            return detail.First();
        }

        // PUT: api/SeatClasses/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public bool PutSeatClass(int id, UpdateSeatClassesInput seatClass)
        {
            var classes = this._context.SeatClasses.Find(id);
            if (classes == null)
            {
                return false;
            }

            classes.ClassId = id;
            classes.Name = seatClass.Name;

            _context.Entry(classes).State = EntityState.Modified;

            return true;
        }

        // POST: api/SeatClasses
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public int PostSeatClass(CreateSeateClassesInput seatClass)
        {
            SeatClass model = new SeatClass
            {
                Name = seatClass.Name
            };

            this._context.SeatClasses.Add(model);
            this._context.SaveChanges();

            return 1;
        }

        // DELETE: api/SeatClasses/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSeatClass(int id)
        {
            var seatClass = await _context.SeatClasses.FindAsync(id);
            if (seatClass == null)
            {
                return NotFound();
            }

            _context.SeatClasses.Remove(seatClass);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
