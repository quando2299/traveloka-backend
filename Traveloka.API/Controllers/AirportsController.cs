﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Traveloka.API.DTO.Airport;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AirportsController : ControllerBase
    {
        private readonly travelokaContext _context;

        public AirportsController(travelokaContext context)
        {
            _context = context;
        }

        // GET: api/Airports
        [HttpGet]
        public IQueryable<AirportDTO> GetAirports()
        {
            var airport = from a in _context.Airports
                          select new AirportDTO()
                          {
                              IataCode = a.IataCode,
                              AirportName = a.AirportName,
                              CountryCode = a.CountryCode
                          };
            return airport;
        }

        // GET: api/Airports/5
        [HttpGet("{id}")]
        public AirportDTO GetAirport(string id)
        {
            var airport = this._context.Airports.ToList();

            var detail = from a in airport
                         where a.IataCode == id
                         select new AirportDTO()
                         {
                             CountryCode = a.CountryCode,
                             AirportName = a.AirportName,
                             IataCode = a.IataCode
                         };

            return detail.First();
        }

        // PUT: api/Airports/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public bool UpdateAirport(string id, UpdateAirportInput airport)
        {
            var _airport = this._context.Airports.Find(id);

            if (_airport == null)
            {
                return false;
            }

            _airport.CountryCode = airport.CountryCode;
            _airport.AirportName = airport.AirportName;
            _airport.IataCode = airport.IataCode;


            this._context.Entry(_airport).State = EntityState.Modified;
            this._context.SaveChanges();

            return true;
        }

        // POST: api/Airports
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public string PostAirport(CreateAirportInput airport)
        {
            Airport newAirport = new Airport
            {
                AirportName = airport.AirportName,
                CountryCode = airport.CountryCode,
                IataCode = airport.IataCode
            };

            this._context.Airports.Add(newAirport);
            this._context.SaveChanges();

            return newAirport.IataCode;
        }

        // DELETE: api/Airports/5
        [HttpDelete("{id}")]
        public bool DeleteAirport(string id)
        {
            var airport = this._context.Airports.Find(id);
            if (airport == null)
            {
                return false;
            }

            this._context.Airports.Remove(airport);
            this._context.SaveChanges();

            return true;
        }
    }
}
