﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Traveloka.API.DTO.RouteStatus;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouteStatusController : ControllerBase
    {
        private readonly travelokaContext _context;

        public RouteStatusController(travelokaContext context)
        {
            _context = context;
        }

        // GET: api/RouteStatus
        [HttpGet]
        public IQueryable<RouteStatusDTO> GetRouteStatuses()
        {
            var routeStatuses = from a in _context.RouteStatuses
                                select new RouteStatusDTO()
                                {
                                    StatusId = a.StatusId,
                                    Name = a.Name
                                };
            return routeStatuses;
        }

        // POST: api/RouteStatus
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public bool PostRouteStatus(CreateRouteStatusInput routeStatus)
        {
            RouteStatus route = new RouteStatus
            {
                Name = routeStatus.Name
            };

            this._context.RouteStatuses.Add(route);
            this._context.SaveChanges();

            return true;
        }
    }
}
