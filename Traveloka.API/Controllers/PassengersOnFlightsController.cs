﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PassengersOnFlightsController : ControllerBase
    {
        private readonly travelokaContext _context;

        public PassengersOnFlightsController(travelokaContext context)
        {
            _context = context;
        }

        // GET: api/PassengersOnFlights
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PassengersOnFlight>>> GetPassengersOnFlights()
        {
            return await _context.PassengersOnFlights.ToListAsync();
        }

        // GET: api/PassengersOnFlights/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PassengersOnFlight>> GetPassengersOnFlight(string id)
        {
            var passengersOnFlight = await _context.PassengersOnFlights.FindAsync(id);

            if (passengersOnFlight == null)
            {
                return NotFound();
            }

            return passengersOnFlight;
        }

        // PUT: api/PassengersOnFlights/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPassengersOnFlight(string id, PassengersOnFlight passengersOnFlight)
        {
            if (id != passengersOnFlight.ItineraryId)
            {
                return BadRequest();
            }

            _context.Entry(passengersOnFlight).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PassengersOnFlightExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PassengersOnFlights
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PassengersOnFlight>> PostPassengersOnFlight(PassengersOnFlight passengersOnFlight)
        {
            _context.PassengersOnFlights.Add(passengersOnFlight);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PassengersOnFlightExists(passengersOnFlight.ItineraryId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPassengersOnFlight", new { id = passengersOnFlight.ItineraryId }, passengersOnFlight);
        }

        // DELETE: api/PassengersOnFlights/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePassengersOnFlight(string id)
        {
            var passengersOnFlight = await _context.PassengersOnFlights.FindAsync(id);
            if (passengersOnFlight == null)
            {
                return NotFound();
            }

            _context.PassengersOnFlights.Remove(passengersOnFlight);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PassengersOnFlightExists(string id)
        {
            return _context.PassengersOnFlights.Any(e => e.ItineraryId == id);
        }
    }
}
