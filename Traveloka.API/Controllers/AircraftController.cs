﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Traveloka.API.DTO.Aircraft;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AircraftController : ControllerBase
    {
        private readonly travelokaContext _context;

        public AircraftController(travelokaContext context)
        {
            _context = context;
        }

        // GET: api/Aircraft
        [Authorize]
        [HttpGet]
        public IQueryable<AircraftDTO> GetAircrafts()
        {
            var aircrafts = from a in _context.Aircrafts
                            select new AircraftDTO()
                            {
                                AircraftId = a.AircraftId,
                                Manufacturer = a.Manufacturer,
                                Model = a.Model
                            };
            return aircrafts;
        }

        // GET: api/Aircraft/5
        [HttpGet("{id}")]
        public AircraftDTO GetAircraft(int id)
        {
            var aircraft = from a in _context.Aircrafts
                           where a.AircraftId == id
                           select new AircraftDTO()
                           {
                               AircraftId = a.AircraftId,
                               Manufacturer = a.Manufacturer,
                               Model = a.Model
                           };

            return aircraft.First();
        }

        // PUT: api/Aircraft/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public bool PutAircraft(int id, UpdateAircraftInput aircraft)
        {
            var _aircraft = this._context.Aircrafts.Find(id);

            if (_aircraft == null)
            {
                return false;
            }

            _aircraft.Manufacturer = aircraft.Manufacturer;
            _aircraft.Model = aircraft.Model;

            this._context.Entry(_aircraft).State = EntityState.Modified;
            this._context.SaveChanges();

            return true;
        }

        // POST: api/Aircraft
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public bool PostAircraft(CreateAircraftInput aircraft)
        {
            var newAircraft = new Aircraft
            {
                Manufacturer = aircraft.Manufacturer,
                Model = aircraft.Model
            };

            this._context.Aircrafts.Add(newAircraft);
            this._context.SaveChanges();

            return true;
        }

        // DELETE: api/Aircraft/5
        [HttpDelete("{id}")]
        public bool DeleteAircraft(int id)
        {
            var aircraft = this._context.Aircrafts.Find(id);

            if (aircraft == null)
            {
                return false;
            }

            this._context.Aircrafts.Remove(aircraft);
            this._context.SaveChanges();

            return true;
        }
    }
}
