﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Traveloka.API.DTO;
using Traveloka.API.DTO.Country;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContriesController : ControllerBase
    {
        private readonly travelokaContext _context;

        public ContriesController(travelokaContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IQueryable<CountryDTO> GetCountries()
        {
            var countries = from a in _context.Countries
                            select new CountryDTO()
                            {
                                CountryCode = a.CountryCode,
                                Name = a.Name
                            };
            return countries;
        }

        [HttpGet("{CountryCode}")]
        public CountryDTO GetCountryByID(string CountryCode)
        {
            var countries = this._context.Countries.ToList();
            var detail = from a in countries
                         where a.CountryCode == CountryCode
                         select new CountryDTO()
                         {
                             CountryCode = a.CountryCode,
                             Name = a.Name
                         };

            return detail.First();
        }

        [HttpPost("CreateCountry")]
        public string PostCountry(CreateCountryInput model)
        {
            Country newCountry = new Country
            {
                Name = model.Name,
                CountryCode = model.CountryCode
            };

            this._context.Countries.Add(newCountry);
            this._context.SaveChanges();

            return model.CountryCode;
        }

        [HttpPut("{CountryCode}")]
        public bool UpdateCountry(string CountryCode, UpdateCountryInput model)
        {
            var country = this._context.Countries.Find(CountryCode);

            if (country == null)
            {
                return false;
            }

            country.CountryCode = CountryCode;
            country.Name = model.Name;
           

            this._context.Entry(country).State = EntityState.Modified;
            this._context.SaveChanges();

            return true;
        }

        [HttpDelete]
        public bool DeleteCountry(string CountryCode)
        {
            var country = this._context.Countries.Find(CountryCode);

            if (country == null)
            {
                return false;
            }

            this._context.Countries.Remove(country);
            this._context.SaveChanges();

            return true;
        }
    }
}
