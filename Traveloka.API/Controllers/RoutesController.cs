﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Traveloka.API.DTO.FullSearch;
using Traveloka.API.DTO.Routes;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoutesController : ControllerBase
    {
        private readonly travelokaContext _context;

        public RoutesController(travelokaContext context)
        {
            _context = context;
        }

        // GET: api/Routes
        [HttpGet]
        public IQueryable<RouteDTO> GetRoutes()
        {
            var routes = from a in _context.Routes
                         select new RouteDTO()
                         {
                             RouteId = a.RouteId,
                             FlightNum = a.FlightNum,
                             OriginAirport = a.OriginAirport,
                             DestinationAirport = a.DestinationAirport,
                             AircraftId = a.AircraftId,
                             DistanceMiles = a.DistanceMiles
                         };
            return routes;
        }

        [HttpGet("{id}")]
        public DetailRouteDTO GetRoute(int id)
        {
            var detailRoute = this._context.Routes.Find(id);
            var timeFlight = this._context.Flights.Where(m => m.RouteId == id).FirstOrDefault();

            var statusFlight = this._context.RouteStatuses.Where(m => m.StatusId == timeFlight.StatusId).FirstOrDefault();

            var originAirport = this._context.Airports.Find(detailRoute.OriginAirport);
            var destiantionAirport = this._context.Airports.Find(detailRoute.DestinationAirport);

            var detail = from a in _context.Routes
                         where a.RouteId == id
                         select new DetailRouteDTO()
                         {
                             RouteId = a.RouteId,
                             fullOriginAirportName = originAirport.AirportName + ", " + originAirport.CountryCode + " (" + originAirport.IataCode + ")", 
                             fullArrivalAirportName = destiantionAirport.AirportName + ", " + destiantionAirport.CountryCode + " (" + destiantionAirport.IataCode + ")",
                             departTime = timeFlight.DepartTimestamp,
                             arriveTime = timeFlight.ArriveTimestamp,
                             Amount = timeFlight.BasePriceUsd,
                             Status = statusFlight.Name
                         };
            return detail.First();
        }

        [HttpPost("GetFullFlight")]
        public object GetFullsearch(string originCode, string arriveCode)
        {
            var originAirport = this._context.Airports.Where(m => m.IataCode == originCode).FirstOrDefault();
            var destinationAirport = this._context.Airports.Where(m => m.IataCode == arriveCode).FirstOrDefault();

            var fullsearch = from a in this._context.Routes
                             join b in this._context.Flights on a.RouteId equals b.RouteId
                             where a.OriginAirport == originCode && a.DestinationAirport == arriveCode
                             select new FullsearchDTO()
                             {
                                 RouteID = a.RouteId,
                                 OriginAirport = originAirport.AirportName,
                                 DestinationAirport = destinationAirport.AirportName,
                                 departTime = b.DepartTimestamp,
                                 arriveTime = b.ArriveTimestamp
                             };
            return fullsearch;
        }


        [HttpPut("{id}")]
        public bool PutRoute(int id, Route route)
        {
            var _route = this._context.Routes.Find(id);

            if (_route == null)
            {
                return false;
            }

            _route.FlightNum = route.FlightNum;
            _route.OriginAirport = route.OriginAirport;
            _route.DestinationAirport = route.DestinationAirport;
            _route.AircraftId = route.AircraftId;
            _route.DistanceMiles = route.DistanceMiles;

            this._context.Entry(_route).State = EntityState.Modified;
            this._context.SaveChanges();

            return true;
        }

        // POST: api/Routes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public bool PostRoute(CreateRouteInput route)
        {
            Route newRoute = new Route
            {
                FlightNum = route.FlightNum,
                OriginAirport = route.OriginAirport,
                DestinationAirport = route.DestinationAirport,
                AircraftId = route.AircraftId,
                DistanceMiles = route.DistanceMiles,
            };
            _context.Routes.Add(newRoute);
            this._context.SaveChanges();

            return true;
        }

        // DELETE: api/Routes/5
        [HttpDelete("{id}")]
        public bool DeleteRoute(int id)
        {
            var route = this._context.Routes.Find(id);

            if (route == null)
            {
                return false;
            }

            this._context.Routes.Remove(route);
            this._context.SaveChanges();

            return true;
        }
    }
}
