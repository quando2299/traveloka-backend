﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Traveloka.API.DTO.UserDTO;
using Traveloka.API.Models;

namespace Traveloka.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly travelokaContext _context;

        public PeopleController(travelokaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public bool Register(Register model)
        {
            if (!checkExistedEmail(model.Email))
            {
                var newUser = new Person
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    LastName = model.LastName,
                    Password = model.Password,
                    PhonePrimary = model.PhonePrimary,
                    DateOfBirth = model.DateOfBirth,
                    Gender = model.Gender,
                };

                this._context.Persons.Add(newUser);
                this._context.SaveChanges();

                return true;
            }

            return false;
        }

        private bool checkExistedEmail(string Email)
        {
            return (this._context.Persons.Where(m => m.Email == Email).FirstOrDefault() != null) ? true : false;
        }
    }
}
