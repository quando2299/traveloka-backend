﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.Airport
{
    public class AirportDTO
    {
        public string IataCode { get; set; }
        public string AirportName { get; set; }
        public string CountryCode { get; set; }
    }
}
