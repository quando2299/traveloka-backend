﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.RouteStatus
{
    public class CreateRouteStatusInput
    {
        public string Name { get; set; }
    }
}
