﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.RouteStatus
{
    public class RouteStatusDTO
    {
        public int StatusId { get; set; }
        public string Name { get; set; }
    }
}
