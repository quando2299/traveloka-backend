﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.FullSearch
{
    public class FullsearchDTO
    {
        public int RouteID { get; set; }
        public string OriginAirport { get; set; }
        public string DestinationAirport { get; set; }
        public DateTime departTime { get; set; }
        public DateTime arriveTime { get; set; }
    }
}
