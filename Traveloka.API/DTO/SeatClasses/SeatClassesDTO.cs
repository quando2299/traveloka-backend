﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.SeatClasses
{
    public class SeatClassesDTO
    {
        public int ClassId { get; set; }
        public string Name { get; set; }
    }
}
