﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.SeatClasses
{
    public class UpdateSeatClassesInput
    {
        public string Name { get; set; }
    }
}
