﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.Transactions
{
    public class DetailTransactionsDTO
    {
        public DateTime? BookingDate { get; set; }
        public int PassengerId { get; set; }
        public int RouteId { get; set; }
        public bool? IsPaid { get; set; }
        public int? PaymentMethod { get; set; }
        public int? Amount { get; set; }
    }
}
