﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.Aircraft
{
    public class AircraftDTO
    {
        public int AircraftId { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
    }
}
