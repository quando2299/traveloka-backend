﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO
{
    public class CountryDTO
    {
        public string CountryCode { get; set; }
        public string Name { get; set; }
    }
}
