﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.Routes
{
    public class UpdateRouteInput
    {
        public short FlightNum { get; set; }
        public string OriginAirport { get; set; }
        public string DestinationAirport { get; set; }
        public int AircraftId { get; set; }
        public short DistanceMiles { get; set; }
    }
}
