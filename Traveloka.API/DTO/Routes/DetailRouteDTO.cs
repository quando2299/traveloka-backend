﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Traveloka.API.DTO.Routes
{
    public class DetailRouteDTO
    {
        public int RouteId { get; set; }
        public string fullOriginAirportName { get; set; }
        public string fullArrivalAirportName { get; set; }
        public dynamic departTime { get; set; }
        public dynamic arriveTime { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
    }
}
